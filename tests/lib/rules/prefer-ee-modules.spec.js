// ------------------------------------------------------------------------------
// Requirements
// ------------------------------------------------------------------------------

const rule = require('../../../lib/rules/prefer-ee-modules');
const RuleTester = require('eslint').RuleTester;

// ------------------------------------------------------------------------------
// Tests
// ------------------------------------------------------------------------------

const ruleTester = new RuleTester({
  // Maybe parser: require.resolve('vue-eslint-parser'),
  parserOptions: { ecmaVersion: 2015 },
});

ruleTester.run('prefer-ee-modules', rule, {
  valid: [
    {
      code: 'alert(1);',
    },
  ],

  invalid: [
    {
      code: 'alert(2);',
      errors: ['Example error'],
    },
  ],
});
