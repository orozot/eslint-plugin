const {
  ERROR_MESSAGE_STRING_LITERAL,
  ERROR_MESSAGE_MISSING_NAMESPACE,
} = require('../../../lib/utils/i18n-utils');

const STRING_LITERALS_VALID_CASES = [
  "__('Bar')",
  "s__('Foo|Bar')",
  "s__('Some Context|Label')",
  "s__('SomeContext', 'The string to be translated')",
  "n__('%d apple', '%d apples', appleCount)",
  's__(`SomeContext|Label`)',
  's__(`Some Context|Label`)',
];
const STRING_LITERALS_INVALID_CASES = [
  '__(foo)',
  '__(getLabel())',
  "s__('SomeContext', label)",
  "n__('%d apple', appleCount)",
];
const STRING_LITERALS_ERRORS = [
  {
    message: ERROR_MESSAGE_STRING_LITERAL,
  },
];

const NAMESPACED_TRANSLATION_VALID_CASES = [
  "s__('Foo|Bar')",
  "s__('SomeContext', 'The string to be translated')",
  `s__(\`
    DastConfig|Customize DAST settings
  \`)`,
];
const NAMESPACED_TRANSLATION_INVALID_CASES = ["s__('FooBar')"];
const NAMESPACED_TRANSLATION_ERRORS = [
  {
    message: ERROR_MESSAGE_MISSING_NAMESPACE,
  },
];

module.exports = {
  STRING_LITERALS_VALID_CASES,
  STRING_LITERALS_INVALID_CASES,
  STRING_LITERALS_ERRORS,
  NAMESPACED_TRANSLATION_VALID_CASES,
  NAMESPACED_TRANSLATION_INVALID_CASES,
  NAMESPACED_TRANSLATION_ERRORS,
};
