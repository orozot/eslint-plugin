// ------------------------------------------------------------------------------
// Requirements
// ------------------------------------------------------------------------------
const path = require('path');
const fs = require('fs');
const { get, isEmpty, findIndex } = require('lodash');

const { DOCS_BASE_URL } = require('../constants');

// ------------------------------------------------------------------------------
//   Helpers
// ------------------------------------------------------------------------------

const ALLOWED_EXTENSIONS = ['.js', '.json', '.vue'];
const getWebpackConfig = (fullPath) => {
  if (!fs.existsSync(fullPath)) {
    throw new Error(`The webpack config file cannot be found in ${fullPath}`);
  }
  return require(fullPath);
};

const getWebpackContext = (configPath) => {
  const fullConfigPath = path.resolve(configPath);
  const config = getWebpackConfig(fullConfigPath);

  const context = get(config, 'context');

  return context || '';
};

const getWebpackAliases = (configPath) => {
  const fullConfigPath = path.resolve(configPath);
  const config = getWebpackConfig(fullConfigPath);

  const aliases = get(config, 'resolve.alias');

  return aliases || {};
};

const getModuleFullPath = ({ currentPath, modulePath, aliasesConfig }) => {
  let fullPath = '';
  const pathList = modulePath.split('/');
  const prefix = pathList[0];

  if (aliasesConfig[prefix]) {
    const aliasPath = aliasesConfig[prefix];

    fullPath = `${aliasPath}/${pathList.slice(1).join('/')}`;
  } else if (modulePath.startsWith('..')) {
    fullPath = path.resolve(currentPath, modulePath);
  }

  if (isEmpty(fullPath)) {
    try {
      fullPath = require.resolve(fullPath);
    } catch (e) {
      fullPath = '';
    }
  }

  return fullPath;
};

const isCEModule = (moduleFullPath, contextPath) => {
  const realPath = require.resolve(moduleFullPath);
  const relativePath = path.relative(contextPath, moduleFullPath);

  const extension = path.extname(realPath);
  const isSupportExtension = findIndex(ALLOWED_EXTENSIONS, (item) => item === extension) > -1;

  const isUnderCEDir =
    relativePath && !relativePath.startsWith('..') && !path.isAbsolute(relativePath);

  return isSupportExtension && isUnderCEDir;
};

const testModuleInEE = (contextPath, ceModulePath, aliasesConfig) => {
  const aliasPath = aliasesConfig['ee'];
  const eeModulePath = ceModulePath.replace(contextPath, aliasPath);
  let eeModuleFullPath = '';

  try {
    eeModuleFullPath = require.resolve(eeModulePath);
  } catch (e) {
    eeModuleFullPath = '';
  }

  return eeModuleFullPath;
};

// ------------------------------------------------------------------------------
// Rule Definition
// ------------------------------------------------------------------------------

module.exports = {
  meta: {
    type: 'error',
    docs: {
      description: 'TODO: Add description',
      category: 'TODO',
      url: DOCS_BASE_URL + '/prefer-ee-modules.md',
    },
    //Optional:
    schema: [],
  },
  create(context) {
    let aliasesConfig = {};
    let contextPath = '';
    const currentPath = path.dirname(context.getFilename());

    if (context && context?.settings) {
      const setting = context?.settings;
      const webpackConfigPath = get(setting, 'import/resolver.webpack.config', '');
      aliasesConfig = getWebpackAliases(webpackConfigPath);
      contextPath = getWebpackContext(webpackConfigPath);
    }

    return {
      ImportDeclaration(node) {
        const modulePath = node.source.value;
        let moduleFullPath = getModuleFullPath({ currentPath, modulePath, aliasesConfig });
        let eeModulePath = '';

        if (moduleFullPath && isCEModule(moduleFullPath, contextPath)) {
          eeModulePath = testModuleInEE(contextPath, moduleFullPath, aliasesConfig);
        }

        if (eeModulePath) {
          context.report({
            node: node,
            message: `The module ${modulePath} is also present in the EE directory at: ${eeModulePath}`,
          });
        }

        return;
      },
    };
  },
};
